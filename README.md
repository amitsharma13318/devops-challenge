# ALTR DevOps Technical Challenge

Welcome to the ALTR DevOps Technical Challenge. 
Below you'll find a series of user stories that test a range of essential DevOps skills. 
Please complete as many user stories as you feel comfortable with. 
Remember, the number of completed stories has no direct relationship with our choice to make an offer.

## Stories

### Cleanup
**Objective**: Improve our GitLab pipeline to handle environment termination efficiently by adding a cleanup process.

**Context**: Our system lacks a cleanup process for terminating GitLab environments, leading to potential resource wastage.

**Given**: The current GitLab pipeline and Terraform configuration.

**When**: A GitLab environment is stopped ('stop' action for environments in GitLab).

**Then**: Initiate a cleanup stage in the GitLab pipeline that properly destroys the Terraform-managed infrastructure, ensuring no residual resources.

**Hint**: Focus on integrating the Terraform destroy command in alignment with the GitLab environment lifecycle.

---

### Make Deploys Faster
**Objective**: Optimize our CI process to reduce deployment times.

**Context**: Deployment is delayed due to unnecessary building of the Go application, even when there are no updates to it.

**Given**: The current Go application and pipeline.

**When**: No updates are made to the Go app but changes are made to Terraform.

**Then**: Bypass the app stages and run only the Terraform stages.

---

### Test the Code Before Deploy
**Objective**: Ensure code quality by integrating tests into the pipeline.

**Context**: Currently, tests can be run locally, but they are not part of the pipeline, leading to potential deployment of faulty code.

**Given**: The current Gitlab pipeline and Go application.

**When**: Tests in the Go app fail.

**Then**: Prevent the pipeline from deploying the code.

---

### Enable DynamoDB
**Objective**: Enable DynamoDB integration for both local development and production.

**Context**: The Go application needs to retrieve values from DynamoDB and return them to the client.

**Given**: The current Go application.

**When**: A request is made to `/dynamo`.

**Then**: The request should return “This was fun!”

---

### Fix the Bug
**Objective**: Identify and resolve existing bugs in the codebase.

**Context**: Our code currently fails some tests. With a working pipeline, these need to be addressed.

**Given**: Documentation for the project.

**Task**: Review the documentation and fix the failing tests.

---

### Multi Region
**Objective**: Enhance infrastructure scalability and resilience by enabling multi-region deployment.

**Context**: Our Terraform code currently supports deployment only in us-east-1.

**Given**: The current Terraform codebase.

**When**: Modifications are made for deployment in us-east-2.

**Then**: Enable successful deployment in both us-east-1 and us-east-2 without impacting existing operations.

**Hint**: Think about modular architecture and safe deployment practices for a scalable solution.

---

## Help
For any questions or assistance, please feel free to reach out. Contact us through moses@altr.com for a prompt response.
