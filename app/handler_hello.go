// package main

// import (
// 	"fmt"
// 	"net/http"
// )

// // helloHandler Prints "Hello, world!" to the response writer on GET requests.
// // On all non GET requests returns a http.StatusMethodNotAllowed.
// func helloHandler(w http.ResponseWriter, r *http.Request) {
// 	switch r.Method {
// 	case http.MethodGet:
// 		_, _ = fmt.Fprint(w, "Hello, Moses!")
// 	default:
// 		http.Error(w, "Method not allowed.", http.StatusMethodNotAllowed)
// 	}
// }


package main

import (
	"net/http"
)

// helloHandler handles requests to the /hello endpoint.
func helloHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed.", http.StatusMethodNotAllowed)
		return
	}
	w.Write([]byte("Hello, World!"))
}
