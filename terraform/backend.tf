terraform {
  backend "s3" {
    bucket = "terraform-tfstate-995"
    key    = "lambda/terraform.tfstate"
    region = "us-east-1"
  }
}